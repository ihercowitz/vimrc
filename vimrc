"****************************************************
"My custom VIMRC file
"
"By Igor Hercowitz
"****************************************************

set runtimepath=~/.vim_runtime,\$VIMRUNTIME,~/.vimfiles

set history=500

filetype plugin on
filetype indent on

"When vimrc is edited, reload it
autocmd! bufwritepost vimrc source ~/.vimrc

au BufRead, BufNewFile *.py set filetype python

au BufRead, BufNewFile *.lisp,*.lsp set filetype lisp 
autocmd BufRead *.lisp,*.lsp  so ~/.vimfiles/syntax/lisp.vim

au BufRead, BufNewFile *.clj set filetype clojure 
autocmd BufRead *.clj  so ~/.vimfiles/syntax/clojure.vim

"Set autoread when a file is changed from the outside
set autoread

syntax enable   "Enable Highlight Syntax
colorscheme zellner
set background=dark
set nu   "Set line number

set encoding=UTF-8
set ffs=unix,dos,mac "Default filetypes

"Turn backup off
set nobackup
set nowb
set noswapfile

"Always show current position
set ruler

"Show match brackets
set showmatch

"Show the current command
set showcmd

set expandtab
set shiftwidth=4
set tabstop=4
set smarttab

set invpaste paste 


"Git branch
function! GitBranch()
    try
        let branch = system("git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* //'")
    catch
        return ''
    endtry

    if branch != ''
        return '   Git Branch: ' . substitute(branch, '\n', '', 'g')
    en

    return ''
endfunction

set laststatus=2
set statusline=
set statusline+=%2*%-3.3n%0*\                " buffer number
set statusline+=%f\                          " file name
set statusline+=%h%1*%m%r%w%0*               " flags
set statusline+=\[%{strlen(&ft)?&ft:'none'}, " filetype
set statusline+=%{&encoding},                " encoding
set statusline+=%{&fileformat}]              " file format
set statusline+=%=                           " right align
"set statusline+=%2*0x%-8B\                   " current char
set statusline+=%-14.(%l,%c%V%)\ %<%P        " offset
set statusline+=%{GitBranch()}\              "Git Branch



"Buffer configuration
map <C-b>  :TMiniBufExplorer<cr>     " Open the minibuffer on top
map <C-b>h :bprev <cr>               " Show the previous buffer
map <C-b>l :bnext <cr>               " Show the next buffer
map <C-b>w :bdelete <cr>             " Close buffer

" Tab configuration
map <C-t>  :tabnew! %<cr>
map <C-t>e :tabedit 
map <C-t>w :tabclose<cr>
map <C-t>g :tabmove 
map <C-t>n :tabn<cr>
map <C-t>p :tabp<cr>

" MiniBufExpl Colors
hi MBEVisibleActive guifg=#A6DB29 guibg=fg
hi MBEVisibleChangedActive guifg=#F1266F guibg=fg
hi MBEVisibleChanged guifg=#F1266F guibg=fg
hi MBEVisibleNormal guifg=#5DC2D6 guibg=fg
hi MBEChanged guifg=#CD5907 guibg=fg
hi MBENormal guifg=#808080 guibg=fg



nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode
